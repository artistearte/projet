<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Validator;
use Auth;

class MainController extends Controller

{
    function index()
    {
        
     return view('index');
    }

    public function list(){

        $data= Http::get('http://api.marketstack.com/v1/tickers?access_key=0a247b9ab9231bde0bd1414eb6f39f2e')->json();

        

        return view('index',['data'=>$data]);

        
    }


    function checklogin(Request $request)
    {
     $this->validate($request, [
      'email'   => 'required|email',
      'password'  => 'required|alphaNum|min:3'
     ]);

     $user_data = array(
      'email'  => $request->get('email'),
      'password' => $request->get('password')
     );

     if(Auth::attempt($user_data))
     {
      return redirect('main');
     }
     else
     {
      return back()->with('error', 'Wrong Login Details');
     }

    }

    function successlogin()
    {
     return view('main');
    }

    function logout()
    {
     Auth::logout();
     return redirect('main');
    }
}

