<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>Projet</title>
  <meta content="" name="description">
  <meta content="" name="keywords">

  <!-- Favicons -->
  <link href="assets/img/favicon.png" rel="icon">
  <link href="assets/img/apple-touch-icon.png" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Jost:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

  <!-- Vendor CSS Files -->
  <link href="assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="assets/vendor/icofont/icofont.min.css" rel="stylesheet">
  <link href="assets/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
  <link href="assets/vendor/remixicon/remixicon.css" rel="stylesheet">
  <link href="assets/vendor/venobox/venobox.css" rel="stylesheet">
  <link href="assets/vendor/owl.carousel/assets/owl.carousel.min.css" rel="stylesheet">
  <link href="assets/vendor/aos/aos.css" rel="stylesheet">

  <!-- Template Main CSS File -->
  <link href="assets/css/style.css" rel="stylesheet">

  <!-- =======================================================
  * Template Name: Arsha - v2.2.0
  * Template URL: https://bootstrapmade.com/arsha-free-bootstrap-html-template-corporate/
  * Author: BootstrapMade.com
  * License: https://bootstrapmade.com/license/
  ======================================================== -->
</head>

<body>

  <!-- ======= Header ======= -->
  <header id="header" class="fixed-top ">
    <div class="container d-flex align-items-center">

      <h1 class="logo mr-auto"><a href="#index">
      <img src="assets/img/logo.jpg" class="img-fluid" alt="">
      
      </a></h1>
      <!-- Uncomment below if you prefer to use an image logo -->
      <!-- <a href="index.html" class="logo mr-auto"><img src="assets/img/logo.png" alt="" class="img-fluid"></a>-->

      <nav class="nav-menu d-none d-lg-block">
        <ul>
          <li class="active"><a href="#hero">Home</a></li>
          <li><a href="#about">Private section 1</a></li>
          <li><a href="#services">Private section 2</a></li>
          <li><a href="#portfolio">Private section 3</a></li>
         
        </ul>
      </nav><!-- .nav-menu -->
      @if(isset(Auth::user()->email))
      <div class="user" style="margin-left: 22px;">
        <strong style="color:white">{{ Auth::user()->name }}</strong>
        </div>
        <div class="logout" style="margin-left: 22px;">
        <a href="{{ url('/main/logout') }}">Logout</a>
        </div>
        @else
      <a href="login" class="get-started-btn scrollto">Login</a>
      @endif
    </div>
  </header><!-- End Header -->

  <!-- ======= Hero Section ======= -->
  <section id="hero" class="d-flex align-items-center">

    <div class="container">
      <div class="row">
        <div class="col-lg-6 d-flex flex-column justify-content-center pt-4 pt-lg-0 order-2 order-lg-1" data-aos="fade-up" data-aos-delay="200">
          <h1>Better Solutions For Your Business</h1>
          <h2>We are team of talanted designers making websites with Bootstrap</h2>
          <div class="d-lg-flex">
            <a href="login" class="btn-get-started scrollto">Login</a>
                       
          
          </div>
        </div>
        <div class="col-lg-6 order-1 order-lg-2 hero-img" data-aos="zoom-in" data-aos-delay="200">
          <img src="assets/img/hero-img.png" class="img-fluid animated" alt="">
        </div>
      </div>
    </div>

  </section><!-- End Hero -->

  <main id="main">

  @if(isset(Auth::user()->email))
    <!-- ======= Private Section 1 ======= -->
    <section id="about" class="about" >
    @else
    <section id="about" class="about" style="Display: none;">
    @endif
      <div class="container" data-aos="fade-up">

        <div class="section-title">
          <h2>Private section 1</h2>
        </div>

        <div class="row content center">
          <div class="col-lg-6 center">
            
            <ul>
              <li><i class="ri-check-double-line"></i> Cours de clôture {{($data['data'][0]['name'])}} le 3 Janvier 2020  </li>
              <li><i class="ri-check-double-line"></i> {{($data['data'][4]['name'])}}</li>
              <li><i class="ri-check-double-line"></i> {{($data['data'][2]['name'])}}</li>
            </ul>
          </div>
          
        </div>

      </div>
    </section><!-- End Private Section 1 -->

    <!-- ======= Private Section 2 ======= -->
    @if(isset(Auth::user()->email))
    <section id="services" class="why-us section-bg" >
    @else
    <section id="services" class="why-us section-bg" style="Display: none;" >
    @endif
      <div class="container-fluid" data-aos="fade-up" >

        <div class="row">

          <div class="col-lg-7 d-flex flex-column justify-content-center align-items-stretch  order-2 order-lg-1">

            <div class="content">
            <div class="section-title">
          <h2>Private section 2</h2>
        </div>
            </div>

            <div class="accordion-list">
              <ul>
                <li>
                  <a data-toggle="collapse" class="collapse" href="#accordion-list-1"><span>01</span> Cours de clôture {{($data['data'][0]['name'])}} le 4 Janvier 2020  <i class="bx bx-chevron-down icon-show"></i><i class="bx bx-chevron-up icon-close"></i></a>
                  <div id="accordion-list-1" class="collapse show" data-parent=".accordion-list">
                   
                  </div>
                </li>

                <li>
                  <a data-toggle="collapse" href="#accordion-list-2" class="collapsed"><span>02</span> {{($data['data'][4]['name'])}} <i class="bx bx-chevron-down icon-show"></i><i class="bx bx-chevron-up icon-close"></i></a>
                  <div id="accordion-list-2" class="collapse" data-parent=".accordion-list">
                   
                  </div>
                </li>

                <li>
                  <a data-toggle="collapse" href="#accordion-list-3" class="collapsed"><span>03</span> {{($data['data'][2]['name'])}} <i class="bx bx-chevron-down icon-show"></i><i class="bx bx-chevron-up icon-close"></i></a>
                  <div id="accordion-list-3" class="collapse" data-parent=".accordion-list">
                   
                  </div>
                </li>

              </ul>
            </div>

          </div>

          <div class="col-lg-5 align-items-stretch order-1 order-lg-2 img" style='background-image: url("assets/img/why-us.png");' data-aos="zoom-in" data-aos-delay="150">&nbsp;</div>
        </div>

      </div>
    </section><!-- Private Section 2 -->

    <!-- ======= Private Section 3 ======= -->
    <section id="portfolio" class="skills">
      <div class="container" data-aos="fade-up">

        <div class="row">
          <div class="col-lg-6 d-flex align-items-center" data-aos="fade-right" data-aos-delay="100">
            <img src="assets/img/skills.png" class="img-fluid" alt="">
          </div>
          <div class="col-lg-6 pt-4 pt-lg-0 content" data-aos="fade-left" data-aos-delay="100">
            <h3>Regarde La video</h3>
            
            <div class="d-lg-flex">
            
          
          </div>
          
            <a href="https://www.youtube.com/embed/sfuYpadW_tM?start=63" class="get-started-btn venobox btn-watch-video" style="color: #37517e;" data-vbtype="video" data-autoplay="true"> Watch Video <i class="icofont-play-alt-2"></i></a>


          </div>
        </div>

      </div>
    </section>

  
  </main><!-- End #main -->

  <!-- ======= Footer ======= -->
  <footer id="footer">

 

    <div class="container footer-bottom clearfix">
      <div class="copyright">
        &copy; Copyright <strong><span>Dropeditions</span></strong>. All Rights Reserved pour ce exam
      </div>
      
      
    </div>
  </footer><!-- End Footer -->

  <a href="#" class="back-to-top"><i class="ri-arrow-up-line"></i></a>
  <div id="preloader"></div>

  <!-- Vendor JS Files -->
  <script src="assets/vendor/jquery/jquery.min.js"></script>
  <script src="assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="assets/vendor/jquery.easing/jquery.easing.min.js"></script>
  <script src="assets/vendor/php-email-form/validate.js"></script>
  <script src="assets/vendor/waypoints/jquery.waypoints.min.js"></script>
  <script src="assets/vendor/isotope-layout/isotope.pkgd.min.js"></script>
  <script src="assets/vendor/venobox/venobox.min.js"></script>
  <script src="assets/vendor/owl.carousel/owl.carousel.min.js"></script>
  <script src="assets/vendor/aos/aos.js"></script>

  <!-- Template Main JS File -->
  <script src="assets/js/main.js"></script>

</body>

</html>